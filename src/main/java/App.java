import java.util.Map;
import java.util.HashMap;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import static spark.Spark.*;

public class App {
  public static void main(String[] args) {
    staticFileLocation("/public");
    String layout = "templates/layout.vtl";

    get("/", (request, response) -> {
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("template", "templates/index.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    get("/bands", (request, response) -> {
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("bands", Band.all());
      model.put("template", "templates/bands.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    get("/bands/new", (request, response) -> {
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("template", "templates/bands-new.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    get("/bands/:id", (request, response) -> {
      Map<String, Object> model = new HashMap<String, Object>();
      Band band = Band.find(Integer.parseInt(request.params("id")));
      model.put("band", band);
      model.put("template", "templates/band.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    post("/bands", (request, response) -> {
      String name = request.queryParams("band-name");
      if (!(name.equals(""))) {
        Band newBand = new Band(name);
        newBand.save();
        response.redirect("/bands/" + newBand.getId());
      } else {
        response.redirect("/bands");
      }
      return null;
    });

    post("/bands/:id/add-venue", (request, response) -> {
      Band newBand = Band.find(Integer.parseInt(request.params("id")));
      String venue = request.queryParams("venue-name");
      if (!(venue.equals(""))) {
        newBand.addVenue(venue);
      }
      response.redirect("/bands/" + newBand.getId());
      return null;
    });

    get("/bands/:band_id/remove-venue/:venue_id", (request, response) -> {
      Band newBand = Band.find(Integer.parseInt(request.params("band_id")));
      Venue newVenue = Venue.find(Integer.parseInt(request.params("venue_id")));
      newBand.removeVenue(newVenue);
      response.redirect("/bands/" + newBand.getId());
      return null;
    });


    get("/bands/:id/delete", (request, response) -> {
      Band newBand = Band.find(Integer.parseInt(request.params("id")));
      newBand.delete();
      response.redirect("/bands");
      return null;
    });

    post("/bands/:id/update", (request, response) -> {
      Band newBand = Band.find(Integer.parseInt(request.params("id")));
      String newName = request.queryParams("band-update-name");
      System.out.println(newName);
      if (!(newName.equals(""))) {
        newBand.update(newName);
      }
      response.redirect("/bands/" + newBand.getId());
      return null;
    });

    get("/venues", (request, response) -> {
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("venues", Venue.all());
      model.put("template", "templates/venues.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    //Why does it matter what order I put the following two routes in. How do I avoid this potential cause of error?

    get("/venues/new", (request, response) -> {
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("template", "templates/venues-new.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    get("/venues/:id", (request, response) -> {
      Map<String, Object> model = new HashMap<String, Object>();
        Venue venue = Venue.find(Integer.parseInt(request.params("id")));
      model.put("venue", venue);
      model.put("template", "templates/venue.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    post("/venues", (request, response) -> {
      String venue = request.queryParams("venue-name");
      if (!(venue.equals(""))) {
        Venue newVenue = new Venue(venue);
        newVenue.save();
        response.redirect("/venues/" + newVenue.getId());
      } else {
        response.redirect("/venues");
      }
      return null;
    });

  }

}
