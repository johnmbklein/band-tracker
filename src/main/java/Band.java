import java.util.List;
import org.sql2o.*;

public class Band {
  private String name;
  private int id;

  public Band(String name){
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public int getId() {
    return id;
  }

  public static List<Band> all() {
    String sql = "SELECT * FROM bands;";
    try(Connection con = DB.sql2o.open()) {
      return con.createQuery(sql).executeAndFetch(Band.class);
    }
  }

  @Override
  public boolean equals(Object otherBand) {
    if (!(otherBand instanceof Band)) {
      return false;
    } else {
      Band newBand = (Band) otherBand;
      return this.getName().equals(newBand.getName());
    }
  }

  public void save() {
    try(Connection con = DB.sql2o.open()) {
      String sql = "INSERT INTO bands (name) VALUES (:name)";
      this.id = (int) con.createQuery(sql, true)
        .addParameter("name", this.name)
        .executeUpdate()
        .getKey();
    }
  }

  public static Band find(int id) {
    try(Connection con = DB.sql2o.open()) {
      String sql = "SELECT * FROM bands WHERE id=:id";
      Band newBand = con.createQuery(sql)
        .addParameter("id", id)
        .executeAndFetchFirst(Band.class); //Why does this have to be executeAndFetchFirst? There should only be one matching entry so why does executeAndFetch cause an error?
      return newBand;
    }
  }

  public void delete() {
    try(Connection con = DB.sql2o.open()) {
      String sql = "DELETE FROM bands WHERE id=:id;";
      sql += "Delete FROM bands_venues WHERE band_id=:id;";
      con.createQuery(sql)
        .addParameter("id", this.id)
        .executeUpdate();
    }
  }

  public void update(String newName) {
    try(Connection con = DB.sql2o.open()) {
      String sql = "UPDATE bands SET name=:name WHERE id=:id;";
      con.createQuery(sql)
        .addParameter("name", newName)
        .addParameter("id", this.id)
        .executeUpdate();
    }
  }

  //Is it best to structure these methods to accept objects as input or other?

  public void addVenue(String venue) {
    Integer venue_id;
    Integer relationshipAlreadyExistsChecker;

    //Check if venue already exists in database
    try(Connection con = DB.sql2o.open()) {
      String sql = "SELECT id FROM venues WHERE name=:name;";
      venue_id = con.createQuery(sql)
        .addParameter("name", venue)
        .executeAndFetchFirst(Integer.class);
    }

    //If venue already exists in database, check to see if relationship already exists in database
    if (venue_id != null) {
      try(Connection con = DB.sql2o.open()) {
        String sql = "SELECT id FROM bands_venues WHERE venue_id=:venue_id AND band_id=:band_id";
        relationshipAlreadyExistsChecker = con.createQuery(sql)
          .addParameter("venue_id", venue_id)
          .addParameter("band_id", this.id)
          .executeAndFetchFirst(Integer.class);
      }

      //If relationship does not already exist in database, add entry to join table
      if (relationshipAlreadyExistsChecker == null) {
        try(Connection con = DB.sql2o.open()) {
          String sql = "INSERT INTO bands_venues (band_id, venue_id) Values (:band_id, :venue_id);";
          con.createQuery(sql)
            .addParameter("band_id", this.id)
            .addParameter("venue_id", venue_id)
            .executeUpdate();
        }
      }

    //If venue does not already exist in database, create venue and add entry to join table
    } else {
      Venue newVenue = new Venue(venue);
      newVenue.save();
      try(Connection con = DB.sql2o.open()) {
        String sql = "INSERT INTO bands_venues (band_id, venue_id) VALUES (:band_id, :venue_id);";
        con.createQuery(sql)
          .addParameter("band_id", this.id)
          .addParameter("venue_id", newVenue.getId())
          .executeUpdate();
      }
    }

  }

  public List<Venue> getVenues() {
    try(Connection con = DB.sql2o.open()) {
      String sql = "SELECT venues.* FROM bands JOIN bands_venues ON (bands.id = bands_venues.band_id) JOIN venues ON (bands_venues.venue_id = venues.id) WHERE bands.id =:id;";
      return con.createQuery(sql)
        .addParameter("id", this.id)
        .executeAndFetch(Venue.class);
    }
  }

  public void removeVenue(Venue venue) {
    try(Connection con =DB.sql2o.open()) {
      String sql = "DELETE FROM bands_venues WHERE band_id=:band_id AND venue_id=:venue_id;";
      con.createQuery(sql)
        .addParameter("band_id", this.id)
        .addParameter("venue_id", venue.getId())
        .executeUpdate();
    }
  }


}
