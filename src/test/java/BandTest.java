import org.sql2o.*;
import org.junit.*;
import static org.junit.Assert.*;

public class BandTest {

  @Rule
  public DatabaseRule database = new DatabaseRule();

  @Test
  public void band_instaiatesCorrectly_true() {
    Band testBand = new Band("The Misfits");
    assertEquals(true, testBand instanceof Band);
  }

  @Test
  public void getName_returnsName_string() {
    Band testBand = new Band("The Misfits");
    assertTrue(testBand.getName().equals("The Misfits"));
  }

  @Test
  public void equals_returnsTrueIfVenueNamesAreSame_true() {
    Venue firstVenue = new Venue("Cat's Cradle");
    Venue secondVenue = new Venue("Cat's Cradle");
    assertTrue(firstVenue.equals(secondVenue));
  }

  @Test
    public void all_emptyAtFirst_0() {
    assertEquals(Band.all().size(), 0);
  }

  @Test
    public void all_returnsAllBands_2() {
    Band firstBand = new Band("The Misfits");
    Band secondBand = new Band("Danzig");
    firstBand.save();
    secondBand.save();
    assertEquals(Band.all().size(), 2);
  }

  @Test
  public void save_savesBandToDatabase_true() {
    Band testBand = new Band("Prince");
    testBand.save();
    Band savedBand = Band.all().get(0);
    assertTrue(testBand.equals(savedBand));
  }

  @Test
  public void save_assignsIdToObject_true() {
    Band myBand = new Band("Queen");
    myBand.save();
    Band savedBand = Band.all().get(0);
    assertEquals(myBand.getId(), savedBand.getId());
  }

  @Test
  public void find_findsBandInDatabase_true() {
    Band myBand = new Band("David Bowie");
    myBand.save();
    Band savedBand = Band.find(myBand.getId());
    assertTrue(myBand.equals(savedBand));
  }

  @Test
  public void delete_deletesBandFromDatabase_true() {
    Band firstBand = new Band("The Misfits");
    Band secondBand = new Band("Danzig");
    firstBand.save();
    secondBand.save();
    firstBand.delete();
    assertTrue(Band.all().get(0).equals(secondBand));
    assertEquals(1, Band.all().size());
  }

  @Test
  public void update_updatesBandNameInDatabase_true() {
    Band testBand = new Band("The Misfits");
    testBand.save();
    testBand.update("Danzig");
    assertTrue(Band.find(testBand.getId()).getName().equals("Danzig"));
  }

  @Test
  public void addVenue_CreatesNewVenueAndAddsToJoinTable_true() {
    Band testBand = new Band("The Misfits");
    testBand.save();
    testBand.addVenue("Cat's Cradle");
    assertTrue(testBand.getVenues().get(0).getName().equals("Cat's Cradle"));
  }

  @Test
  public void addVenue_doesNotCreateExtantVenueAndAddsToJoinTable_true() {
    Band testBand = new Band("The Misfits");
    testBand.save();
    Venue testVenue = new Venue("Cat's Cradle");
    testVenue.save();
    testBand.addVenue("Cat's Cradle");
    assertEquals(1, Venue.all().size());
    assertTrue(testBand.getVenues().contains(testVenue));
  }

  @Test
  public void addVenue_doesNotCreateDuplicateInJoinTable_1() {
    Band testBand = new Band("The Misfits");
    testBand.save();
    Venue testVenue = new Venue("Cat's Cradle");
    testVenue.save();
    testBand.addVenue("Cat's Cradle");
    testBand.addVenue("Cat's Cradle");
    assertEquals(1, Venue.all().size());
    assertEquals(1, testBand.getVenues().size());
  }

  @Test
  public void getVenues_returesAllVenues_true() {
    Band testBand = new Band("Queen");
    testBand.save();
    Venue firstVenue = new Venue("Cat's Cradle");
    Venue secondVenue = new Venue("Local 506");
    testBand.addVenue("Cat's Cradle");
    testBand.addVenue("Local 506");
    assertTrue(testBand.getVenues().contains(firstVenue) && testBand.getVenues().contains(secondVenue));
  }

  @Test
  public void removeVenues_removesEntryFromJoinTable_true() {
    Band testBand = new Band("Queen");
    testBand.save();
    Venue firstVenue = new Venue("Cat's Cradle");
    Venue secondVenue = new Venue("Local 506");
    firstVenue.save();
    secondVenue.save();
    testBand.addVenue("Cat's Cradle");
    testBand.addVenue("Local 506");
    testBand.removeVenue(firstVenue);
    assertTrue(!(testBand.getVenues().contains(firstVenue)) && testBand.getVenues().contains(secondVenue));
  }



}
