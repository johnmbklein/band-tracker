import org.sql2o.*;
import org.junit.*; // for @Before and @After
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import org.fluentlenium.adapter.FluentTest;
import static org.fluentlenium.core.filter.FilterConstructor.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class AppTest extends FluentTest {
  public WebDriver webDriver = new HtmlUnitDriver();

  @Override
  public WebDriver getDefaultDriver() {
    return webDriver;
  }


  @ClassRule
  public static ServerRule server = new ServerRule();

  @Rule
  public DatabaseRule database = new DatabaseRule();

  @Test
  public void rootTest() {
    goTo("http://localhost:4567/");
    assertThat(pageSource()).contains("I'm an index");
  }

  @Test
  public void bandsPageDisplaysAllBands() {
    Band firstBand = new Band("Prince");
    Band secondBand = new Band("Queen");
    firstBand.save();
    secondBand.save();
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    assertThat(pageSource()).contains("Prince");
    assertThat(pageSource()).contains("Queen");
  }

  @Test
  public void bandPageDisplaysAllVenues() {
    Band testBand = new Band("Queen");
    testBand.save();
    testBand.addVenue("Cat's Cradle");
    testBand.addVenue("Local 506");
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    click("a", withText("Queen"));
    assertThat(pageSource()).contains("Cat's Cradle");
    assertThat(pageSource()).contains("Local 506");
  }

  @Test
  public void newBandIsAddedAndDisplayed() {
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    click("a", withText("Add a New Band"));
    fill("#band-name").with("Ratatat");
    submit("#new-band-submit");
    assertThat(pageSource()).contains("Ratatat");
  }

  @Test
  public void bandWithEmptyNameIsIgnored() {
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    click("a", withText("Add a New Band"));
    fill("#band-name").with("");
    submit("#new-band-submit");
    assertEquals(0, Band.all().size());
  }

  @Test
  public void venueIsAddedToBand() {
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    click("a", withText("Add a New Band"));
    fill("#band-name").with("Ratatat");
    submit("#new-band-submit");
    fill("#venue-name").with("Cat's Cradle");
    submit("#new-venue-submit");
    assertThat(pageSource()).contains("Cat's Cradle");
  }

  @Test
  public void venueIsRemovedFromBand() {
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    click("a", withText("Add a New Band"));
    fill("#band-name").with("Ratatat");
    submit("#new-band-submit");
    fill("#venue-name").with("Cat's Cradle");
    submit("#new-venue-submit");
    click("a", withText("Remove this venue"));
    assertFalse(pageSource().contains("Cat's Cradle") && pageSource().contains("Ratatat"));
  }

  @Test
  public void venueAddToBandWithEmptyNameIsIgnored() {
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    click("a", withText("Add a New Band"));
    fill("#band-name").with("Ratatat");
    submit("#new-band-submit");
    fill("#venue-name").with("");
    submit("#new-venue-submit");
    assertEquals(0, Venue.all().size());
  }

  @Test
  public void bandNameIsUpdated() {
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    click("a", withText("Add a New Band"));
    fill("#band-name").with("Ratatat");
    submit("#new-band-submit");
    fill("#band-update-name").with("Kraftwerk");
    submit("#band-update-name-submit");
    assertThat(pageSource()).contains("Kraftwerk");
  }

  @Test
  public void updateBandNameIgnoredIfEmpty() {
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    click("a", withText("Add a New Band"));
    fill("#band-name").with("Ratatat");
    submit("#new-band-submit");
    fill("#band-update-name").with("");
    submit("#band-update-name-submit");
    assertThat(pageSource()).contains("Ratatat");
  }

  @Test
  public void bandIsDeleted() {
    goTo("http://localhost:4567/");
    click("a", withText("Bands"));
    click("a", withText("Add a New Band"));
    fill("#band-name").with("Ratatat");
    submit("#new-band-submit");
    click("a", withText("Delete Ratatat"));
    assertThat(!(pageSource()).contains("Ratatat"));
  }

  @Test
  public void venuesPageDisplaysAllBands() {
    Venue firstVenue = new Venue("Cat's Cradle");
    Venue secondVenue = new Venue("Local 506");
    firstVenue.save();
    secondVenue.save();
    goTo("http://localhost:4567/");
    click("a", withText("Venues"));
    assertThat(pageSource()).contains("Cat's Cradle");
    assertThat(pageSource()).contains("Local 506");
  }

  @Test
  public void venuePageDisplaysAllBands() {
    Band firstBand = new Band("Prince");
    Band secondBand = new Band("Queen");
    firstBand.save();
    secondBand.save();
    firstBand.addVenue("Cat's Cradle");
    secondBand.addVenue("Cat's Cradle");
    goTo("http://localhost:4567/");
    click("a", withText("Venues"));
    click("a", withText("Cat's Cradle"));
    assertThat(pageSource()).contains("Prince");
    assertThat(pageSource()).contains("Queen");
  }

  @Test
  public void newVenueIsAddedAndDisplayed() {
    goTo("http://localhost:4567/");
    click("a", withText("Venues"));
    click("a", withText("Add a New Venue"));
    fill("#venue-name").with("Cat's Cradle");
    submit("#new-venue-submit");
    assertThat(pageSource()).contains("Cat's Cradle");
  }

  @Test
  public void venueWithEmptyNameIsIgnored() {
    goTo("http://localhost:4567/");
    click("a", withText("Venues"));
    click("a", withText("Add a New Venue"));
    fill("#venue-name").with("");
    submit("#new-venue-submit");
    assertEquals(0, Venue.all().size());
  }


}
