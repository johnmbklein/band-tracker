import org.sql2o.*;
import org.junit.*;
import static org.junit.Assert.*;

public class VenueTest {

  @Rule
  public DatabaseRule database = new DatabaseRule();

  @Test
  public void venue_venueInstantiatesCorrectly_true() {
    Venue testVenue = new Venue("Cat's Cradle");
    assertEquals(true, testVenue instanceof Venue);
  }

  @Test
  public void getName_returnsName_string() {
    Venue testVenue = new Venue("Cat's Cradle");
    assertTrue(testVenue.getName().equals("Cat's Cradle"));
  }

  @Test
    public void all_emptyAtFirst_true() {
    assertEquals(Venue.all().size(), 0);
  }

  @Test
    public void all_returnsAllVenues_2() {
    Venue firstVenue = new Venue("Cat's Cradle");
    Venue secondVenue = new Venue("Local 506");
    firstVenue.save();
    secondVenue.save();
    assertEquals(Venue.all().size(), 2);
  }

  @Test
  public void equals_returnsTrueIfVenueNamesAretheSame() {
    Venue firstVenue = new Venue("Cat's Cradle");
    Venue secondVenue = new Venue("Cat's Cradle");
    assertTrue(firstVenue.equals(secondVenue));
  }

  @Test
  public void save_savesVenueToDatabase_true() {
    Venue testVenue = new Venue("Cat's Cradle");
    testVenue.save();
    Venue savedVenue = Venue.all().get(0);
    assertTrue(testVenue.equals(savedVenue));
  }

  @Test
  public void save_assignsIdToObject_true() {
    Venue myVenue = new Venue("Cat's Cradle");
    myVenue.save();
    Venue savedVenue = Venue.all().get(0);
    assertEquals(myVenue.getId(), savedVenue.getId());
  }

  @Test
  public void find_findsVenueInDatabase_true() {
    Venue myVenue = new Venue("Cat's Cradle");
    myVenue.save();
    Venue savedVenue = Venue.find(myVenue.getId());
    assertTrue(myVenue.equals(savedVenue));
  }

  @Test
  public void delete_deletesVenueFromDatabase_true() {
    Venue firstVenue = new Venue("Cat's Cradle");
    Venue secondVenue = new Venue("Local 506");
    firstVenue.save();
    secondVenue.save();
    firstVenue.delete();
    assertTrue(Venue.all().get(0).equals(secondVenue));
    assertEquals(1, Venue.all().size());
  }

  @Test
  public void update_updatesVenueNameInDatabase_true() {
    Venue testVenue = new Venue("Cat's Cradle");
    testVenue.save();
    testVenue.update("Local 506");
    assertTrue(Venue.find(testVenue.getId()).getName().equals("Local 506"));
  }

  @Test
  public void getBands_returnsAllBands_true() {
    Venue testVenue = new Venue("Cat's Cradle");
    testVenue.save();
    Band firstBand = new Band("The Police");
    firstBand.save();
    Band secondBand = new Band("Iggy Pop");
    secondBand.save();
    firstBand.addVenue("Cat's Cradle");
    secondBand.addVenue("Cat's Cradle");
    //Better to run as one assertion or to split up into multiple assertions? 
    assertTrue(testVenue.getBands().contains(firstBand) && testVenue.getBands().contains(secondBand) && testVenue.getBands().size()==2);
  }

}
